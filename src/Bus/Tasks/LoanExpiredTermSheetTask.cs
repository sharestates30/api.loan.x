﻿using Bus.Messages;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Topshelf;

namespace Bus.Tasks
{
    public class LoanExpiredTermSheetTask : IJob
    {

        private readonly CloudDataService _cloudDataService;

        public LoanExpiredTermSheetTask(CloudDataService cloudDataService)
        {
            this._cloudDataService = cloudDataService;
        }

        public void Execute(IJobExecutionContext context)
        {
            var queue = this._cloudDataService.GetQueue(QueueNames.LoanExpiredTermSheetQueue);
            var claim = this._cloudDataService.ReceiveMessage(queue).GetAwaiter().GetResult();

            this._cloudDataService.ProcessMessage<LoanMessage>(queue, claim, (message) =>
            {


            }).GetAwaiter().GetResult();

        }
    }
}
