﻿using Bus.Messages;
using Bus.Models;
using Bus.Services;
using Newtonsoft.Json;
using Quartz;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Topshelf;

namespace Bus.Tasks
{
    public class LoanPendingTask : IJob
    {

        private readonly CloudDataService _cloudDataService;
        private readonly ITokenService _tokenService;
        private readonly ILoanResolverService _loanResolver;
        private readonly ISendEmailService _sendEmailService;

        public LoanPendingTask(CloudDataService cloudDataService, ITokenService tokenService, ILoanResolverService loanResolver, ISendEmailService sendEmailService)
        {
            this._cloudDataService = cloudDataService;
            this._tokenService = tokenService;
            this._loanResolver = loanResolver;
            this._sendEmailService = sendEmailService;
        }

        public void Execute(IJobExecutionContext context)
        {
            var queue = this._cloudDataService.GetQueue(QueueNames.LoanPendingQueue);
            var claim = this._cloudDataService.ReceiveMessage(queue).GetAwaiter().GetResult();

            this._cloudDataService.ProcessMessage<LoanMessage>(queue, claim, (message) =>
            {
                // Get token
                var token = this._tokenService.GetToken().GetAwaiter().GetResult();

                var loanApi = System.Configuration.ConfigurationManager.AppSettings["loanApi"];
                var storageApi = System.Configuration.ConfigurationManager.AppSettings["storageApi"];

                var loan = this._loanResolver.GetLoan(message.LoanId, token).GetAwaiter().GetResult();

                // Upload PDF
                var pdf = this._loanResolver.GetLoanPdf(message.LoanId, token).GetAwaiter().GetResult();

                MultipartFormDataContent formData = new MultipartFormDataContent();
                formData.Add(new StreamContent(new MemoryStream(pdf)), "files", $"{loan.Loan.LoanNumber}.pdf");
                formData.Add(new StringContent("UserId"), message.DeveloperId);

                var httpClient = new HttpClient();
                httpClient.SetBearerToken(token.access_token);
                var responseFiles = httpClient.PostAsync($"{storageApi}/file", formData).GetAwaiter().GetResult();
                var files = responseFiles.ReadAsObject<IReadOnlyList<FileViewModel>>();

                var request = new
                {
                    LoanApplicationFormFileId = files.ElementAt(0).FileItemId.ToString(),
                    LoanApplicationFormFileName = files.ElementAt(0).FileName,
                    LoanApplicationFormFileLink = files.ElementAt(0).FileUri,
                };

                // Upload Loan
                var response = httpClient.PutAsync($"{loanApi}/loan/{message.LoanId}/uploadLoanForm", new StringContent(JsonConvert.SerializeObject(request), Encoding.UTF8, "application/json")).GetAwaiter().GetResult();

                if (!response.IsSuccessStatusCode)
                    throw new Exception(response.Content.ReadAsStringAsync().Result);

                // notify to borrower and admin.
                var emails = new List<RecipientViewModel>();
                emails.Add(new RecipientViewModel { Email = loan.Developer.Email, FirstName = loan.Developer.FirstName, LastName = loan.Developer.LastName });

                if (loan.Broker != null)
                    emails.Add(new RecipientViewModel { Email = loan.Broker.Email, FirstName = loan.Broker.FirstName, LastName = loan.Broker.LastName });

                var model = new List<DataModelMessage>();
                this._sendEmailService.Send(emails, model, "LoanPending").GetAwaiter().GetResult();
                

            }).GetAwaiter().GetResult();

        }
    }
}
