﻿using Bus.Messages;
using Bus.Models;
using Bus.Services;
using Newtonsoft.Json;
using Quartz;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Topshelf;

namespace Bus.Tasks
{
    public class LoanPendingSignatureTermSheetTask : IJob
    {
        private readonly CloudDataService _cloudDataService;
        private readonly ITokenService _tokenService;
        private readonly ILoanResolverService _loanResolver;

        public LoanPendingSignatureTermSheetTask(CloudDataService cloudDataService, ITokenService tokenService, ILoanResolverService loanResolver)
        {
            this._cloudDataService = cloudDataService;
            this._tokenService = tokenService;
            this._loanResolver = loanResolver;
        }

        public void Execute(IJobExecutionContext context)
        {
            var queue = this._cloudDataService.GetQueue(QueueNames.LoanPendingSignatureTermSheetQueue);
            var claim = this._cloudDataService.ReceiveMessage(queue).GetAwaiter().GetResult();

            this._cloudDataService.ProcessMessage<LoanPendingTermSheetMessage>(queue, claim, (message) =>
            {
                // Get token
                var token = this._tokenService.GetToken().GetAwaiter().GetResult();

                // Get api url
                var loanApi = System.Configuration.ConfigurationManager.AppSettings["loanApi"];
                var signApi = System.Configuration.ConfigurationManager.AppSettings["signApi"];
                var storageApi = System.Configuration.ConfigurationManager.AppSettings["storageApi"];

                var httpClient = new HttpClient();
                httpClient.SetBearerToken(token.access_token);

                // Get Loan
                var loan = this._loanResolver.GetLoan(message.LoanId, token).GetAwaiter().GetResult();

                // upload file to storage.
                var responseFile = httpClient.GetAsync($"{signApi}/sign/termsheet/pending/download/{message.FileName}").GetAwaiter().GetResult();
                var fileBytes = responseFile.Content.ReadAsByteArrayAsync().GetAwaiter().GetResult();
                MultipartFormDataContent formData = new MultipartFormDataContent();
                formData.Add(new StreamContent(new MemoryStream(fileBytes)), "files", message.FileName);
                formData.Add(new StringContent("UserId"), message.DeveloperId);

                var responseFiles = httpClient.PostAsync($"{storageApi}/file", formData).GetAwaiter().GetResult();
                var files = responseFiles.ReadAsObject<IReadOnlyList<FileViewModel>>();

                var request = new
                {
                    TermSheetFileId = files.ElementAt(0).FileItemId.ToString(),
                    TermSheetFileName = files.ElementAt(0).FileName,
                    TermSheetFileLink = files.ElementAt(0).FileUri,
                };

                // Mark loan to Term Sheet Under Review
                var response = httpClient.PutAsync($"{loanApi}/loan/{message.LoanId}/termSheetPendingSignature", new StringContent(JsonConvert.SerializeObject(request), Encoding.UTF8, "application/json")).GetAwaiter().GetResult();

                if (!response.IsSuccessStatusCode)
                    throw new Exception(response.Content.ReadAsStringAsync().Result);


            }).GetAwaiter().GetResult();
        }
    }
}
