﻿using Bus.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Dynamic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Bus.Services
{
    public class SendEmailService : ISendEmailService
    {
        private readonly CloudDataService _cloudDataService;

        public SendEmailService(CloudDataService cloudDataService)
        {
            this._cloudDataService = cloudDataService;
        }

        public async Task Send(List<RecipientViewModel> recipients, List<DataModelMessage> model, string notificationTemplateCode)
        {
            dynamic jsonObject = JObject.Parse(Newtonsoft.Json.JsonConvert.SerializeObject(new
            {
                NotificationTemplateCode = notificationTemplateCode,
                Recipients = recipients,
                Model = model
            }));

            var queuePendingSignature = this._cloudDataService.GetQueue(ExternalQueueNames.NotificationEmailDelivery);
            await this._cloudDataService.SendMessage(queuePendingSignature, jsonObject);
        }
    }
}
