﻿using Bus.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bus.Services
{
    public interface ILoanResolverService
    {
        Task<LoanInfoViewModel> GetLoan(Guid loanId, OAuthToken token);
        Task<byte[]> GetLoanPdf(Guid loanId, OAuthToken token);
    }
}
