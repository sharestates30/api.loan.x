﻿using net.openstack.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bus
{
    internal class BusServiceManager 
    {
        private readonly CloudDataService _cloudService;

        public BusServiceManager()
        {
            this._cloudService = new CloudDataService();
        }

        public bool Start()
        {
            // Configure Queues.

            this._cloudService.DeleteQueue(QueueNames.LoanPendingToAuthorizeQueue).GetAwaiter().GetResult();
            this._cloudService.DeleteQueue(QueueNames.LoanPendingQueue).GetAwaiter().GetResult();
            this._cloudService.DeleteQueue(QueueNames.LoanAcceptedQueue).GetAwaiter().GetResult();
            this._cloudService.DeleteQueue(QueueNames.LoanSignedTermSheetQueue).GetAwaiter().GetResult();
            this._cloudService.DeleteQueue(QueueNames.LoanExpiredTermSheetQueue).GetAwaiter().GetResult();
            this._cloudService.DeleteQueue(QueueNames.LoanAcceptedWithDepositQueue).GetAwaiter().GetResult();
            this._cloudService.DeleteQueue(QueueNames.LoanAppraisalOrderedQueue).GetAwaiter().GetResult();
            this._cloudService.DeleteQueue(QueueNames.LoanAppraisalReceivedQueue).GetAwaiter().GetResult();
            this._cloudService.DeleteQueue(QueueNames.LoanAppraisalReportSentQueue).GetAwaiter().GetResult();
            this._cloudService.DeleteQueue(QueueNames.LoanAppraisalApprovedQueue).GetAwaiter().GetResult();
            this._cloudService.DeleteQueue(QueueNames.LoanRejectQueue).GetAwaiter().GetResult();
            this._cloudService.DeleteQueue(QueueNames.LoanCommentQueue).GetAwaiter().GetResult();
            this._cloudService.DeleteQueue(QueueNames.LoanUnderReviewTermSheetQueue).GetAwaiter().GetResult();
            this._cloudService.DeleteQueue(QueueNames.LoanPendingSignatureTermSheetQueue).GetAwaiter().GetResult();
            this._cloudService.DeleteQueue(QueueNames.LoanTitleRequiredQueue).GetAwaiter().GetResult();
            this._cloudService.DeleteQueue(QueueNames.LoanUWClearQueue).GetAwaiter().GetResult();
            this._cloudService.DeleteQueue(QueueNames.LoanUWTitleClearQueue).GetAwaiter().GetResult();
            this._cloudService.DeleteQueue(QueueNames.LoanTitleReceivedQueue).GetAwaiter().GetResult();
            this._cloudService.DeleteQueue(QueueNames.LoanLenderInstructionsIssuedQueue).GetAwaiter().GetResult();
            this._cloudService.DeleteQueue(QueueNames.LoanClosedQueue).GetAwaiter().GetResult();
            this._cloudService.DeleteQueue(QueueNames.LoanAssignUserQueue).GetAwaiter().GetResult();
            this._cloudService.DeleteQueue(QueueNames.LoanNotificationQueue).GetAwaiter().GetResult();
            this._cloudService.DeleteQueue(QueueNames.LoanDocumentChangeStatusQueue).GetAwaiter().GetResult();
            this._cloudService.DeleteQueue(QueueNames.LoanCreateNotificationQueue).GetAwaiter().GetResult();


            this._cloudService.CreateQueue(QueueNames.LoanPendingToAuthorizeQueue).GetAwaiter().GetResult();
            this._cloudService.CreateQueue(QueueNames.LoanPendingQueue).GetAwaiter().GetResult();
            this._cloudService.CreateQueue(QueueNames.LoanAcceptedQueue).GetAwaiter().GetResult();
            this._cloudService.CreateQueue(QueueNames.LoanSignedTermSheetQueue).GetAwaiter().GetResult();
            this._cloudService.CreateQueue(QueueNames.LoanExpiredTermSheetQueue).GetAwaiter().GetResult();
            this._cloudService.CreateQueue(QueueNames.LoanAcceptedWithDepositQueue).GetAwaiter().GetResult();
            this._cloudService.CreateQueue(QueueNames.LoanAppraisalOrderedQueue).GetAwaiter().GetResult();
            this._cloudService.CreateQueue(QueueNames.LoanAppraisalReceivedQueue).GetAwaiter().GetResult();
            this._cloudService.CreateQueue(QueueNames.LoanAppraisalReportSentQueue).GetAwaiter().GetResult();
            this._cloudService.CreateQueue(QueueNames.LoanAppraisalApprovedQueue).GetAwaiter().GetResult();
            this._cloudService.CreateQueue(QueueNames.LoanRejectQueue).GetAwaiter().GetResult();
            this._cloudService.CreateQueue(QueueNames.LoanCommentQueue).GetAwaiter().GetResult();
            this._cloudService.CreateQueue(QueueNames.LoanUnderReviewTermSheetQueue).GetAwaiter().GetResult();
            this._cloudService.CreateQueue(QueueNames.LoanPendingSignatureTermSheetQueue).GetAwaiter().GetResult();
            this._cloudService.CreateQueue(QueueNames.LoanTitleRequiredQueue).GetAwaiter().GetResult();
            this._cloudService.CreateQueue(QueueNames.LoanUWClearQueue).GetAwaiter().GetResult();
            this._cloudService.CreateQueue(QueueNames.LoanUWTitleClearQueue).GetAwaiter().GetResult();
            this._cloudService.CreateQueue(QueueNames.LoanTitleReceivedQueue).GetAwaiter().GetResult();
            this._cloudService.CreateQueue(QueueNames.LoanLenderInstructionsIssuedQueue).GetAwaiter().GetResult();
            this._cloudService.CreateQueue(QueueNames.LoanClosedQueue).GetAwaiter().GetResult();
            this._cloudService.CreateQueue(QueueNames.LoanAssignUserQueue).GetAwaiter().GetResult();
            this._cloudService.CreateQueue(QueueNames.LoanNotificationQueue).GetAwaiter().GetResult();
            this._cloudService.CreateQueue(QueueNames.LoanDocumentChangeStatusQueue).GetAwaiter().GetResult();
            this._cloudService.CreateQueue(QueueNames.LoanCreateNotificationQueue).GetAwaiter().GetResult();

            return true;
        }

        public bool Stop()
        {
            return true;
        }
    }
}
