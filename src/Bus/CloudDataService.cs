﻿using net.openstack.Providers.Rackspace;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using net.openstack.Core.Domain;
using net.openstack.Core.Domain.Queues;
using System.Threading;

namespace Bus
{
    public class CloudDataService
    {

        public string UserName { get { return ConfigurationManager.AppSettings["rackSpaceUserName"]; } }
        public string ApiKey { get { return ConfigurationManager.AppSettings["rackSpaceApiKey"]; } }
        public string Region { get { return ConfigurationManager.AppSettings["rackSpaceRegion"]; } }

        private CloudQueuesProvider GetProvider()
        {
            var cloudIdentity = new CloudIdentity
            {
                Username = UserName,
                APIKey = ApiKey
            };

            var identityService = new CloudIdentityProvider(cloudIdentity);

            CloudQueuesProvider cloudQueuesProvider = new CloudQueuesProvider(cloudIdentity, Region, Guid.NewGuid(), false, null);

            return cloudQueuesProvider;
        }


        private string ResolveQueuePrefix(string queueName)
        {
            if (ConfigurationManager.AppSettings["environment"].Equals("Development", StringComparison.OrdinalIgnoreCase))
                return $"dev-{queueName}";

            if (ConfigurationManager.AppSettings["environment"].Equals("QA", StringComparison.OrdinalIgnoreCase))
                return $"qa-{queueName}";

            return queueName;
        }

        public QueueName GetQueue(string name) {
            QueueName queueName = new QueueName(ResolveQueuePrefix(name));

            return queueName;
        }

        public async Task CreateQueue(string queueName) {

            var provider = GetProvider();

            queueName = ResolveQueuePrefix(queueName);

            QueueName queue = new QueueName(queueName);

            try
            {
                bool created = await provider.CreateQueueAsync(queue, CancellationToken.None);

                if (created)
                    Console.WriteLine($"Queue [{queueName}] created");

                if (!created)
                    Console.WriteLine($"Queue [{queueName}] exists");
                //if (!await provider.QueueExistsAsync(queue, CancellationToken.None))
                //{
                  
                //}
                //else
                //{
                    
                //}
            }
            catch (Exception ex)
            {

                throw;
            }

        }

        public async Task DeleteQueue(string queueName)
        {
            var provider = GetProvider();

            queueName = ResolveQueuePrefix(queueName);

            QueueName queue = new QueueName(queueName);
            await provider.DeleteQueueAsync(queue, CancellationToken.None);
            Console.WriteLine($"Queue [{queueName}] deleted");
        }

        public async Task SendMessage(QueueName queue, dynamic body) {

            TimeSpan ttl = TimeSpan.FromMinutes(900);
            Message message = new Message(ttl, body);
            Message[] messages = { message };

            await GetProvider().PostMessagesAsync(queue, CancellationToken.None, messages);

        }

        public async Task<Claim> ReceiveMessage(QueueName queue) {

            TimeSpan ttl = TimeSpan.FromMinutes(1);
            TimeSpan grace = TimeSpan.FromMinutes(120);

            Claim claim = await GetProvider().ClaimMessageAsync(
                 queue,
                 null,
                 ttl,
                 grace,
                 CancellationToken.None);

            return claim;

        }

        public async Task ProcessMessage<T>(QueueName queue, Claim claim, Action<T> action) {

            if (claim != null && claim.Messages != null) {
                foreach (var item in claim.Messages)
                {
                    try
                    {
                        // Process messages.
                        Console.WriteLine($"Process message  => {item.Id.Value}");
                        var message = item.Body.ToObject<T>();
                        action(message);
                        await RemoveMessage(queue, claim, item.Id);
                        Console.WriteLine($"Done process message  => {item.Id.Value}");
                    }
                    catch (Exception ex)
                    {
                        await ReleaseMessage(queue, claim);
                        Console.WriteLine($"Fail process message  => {item.Id.Value}");
                    }
                }
            }

        }

        public async Task ReleaseMessage(QueueName queue, Claim claim)
        {
            await GetProvider().ReleaseClaimAsync(queue, claim, CancellationToken.None);
        }

        public async Task RemoveMessage(QueueName queue, Claim claim, MessageId messageId)
        {
            await GetProvider().DeleteMessageAsync(queue, messageId, claim, CancellationToken.None);
        }

    }
}
