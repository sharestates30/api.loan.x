﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Bus
{
    public static class HttpResponseMessageExtension
    {
        public static T ReadAsObject<T>(this HttpResponseMessage content) {
            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(content.Content.ReadAsStringAsync().Result);
        }
    }
}
