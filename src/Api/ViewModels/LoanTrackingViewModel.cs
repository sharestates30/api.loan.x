﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.ViewModels
{
    public class LoanTrackingViewModel
    {
        public int LoanTrackingId { get; set; }
        public Guid LoanId { get; set; }
        public string Status { get; set; }
        public string StatusName { get; set; }
        public string Code { get; set; }
        public string Content { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedByName { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
