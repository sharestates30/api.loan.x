﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.ViewModels
{
    public class LoandChangeDocumentStatusPostViewModel
    {
        /// <summary>
        /// 1 => Partial
        /// 2 => Completed
        /// 3 => Pending
        /// 4 => Cleared
        /// </summary>
        public int Status { get; set; }

        public string Comment { get; set; }

        public List<FilePostViewModel> Files { get; set; }
    }
}
