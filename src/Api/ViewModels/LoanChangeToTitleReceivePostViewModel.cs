﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Api.ViewModels
{
    public class LoanChangeToTitleReceivePostViewModel
    {

        /// <summary>
        /// Documents : Title Application Form
        /// </summary>
        [Required]
        public string TitleApplicationFormFileId { get; set; }
        [Required]
        public string TitleApplicationFormFileName { get; set; }
        [Required]
        public string TitleApplicationFormFileLink { get; set; }

        /// <summary>
        /// Documents : Title Report
        /// </summary>
        [Required]
        public string TitleReportFileId { get; set; }
        [Required]
        public string TitleReportFileName { get; set; }
        [Required]
        public string TitleReportFileLink { get; set; }

        /// <summary>
        /// Documents : Title Policy
        /// </summary>
        [Required]
        public string TitlePolicyFileId { get; set; }
        [Required]
        public string TitlePolicyFileName { get; set; }
        [Required]
        public string TitlePolicyFileLink { get; set; }

        public string Comment { get; set; }
    }
}
