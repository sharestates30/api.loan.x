﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Api.ViewModels
{
    public class LoanChangeToAcceptedPostViewModel
    {
        ///// <summary>
        ///// Documents : Loan Application Form
        ///// </summary>
        //[Required]
        //public string LoanApplicationFormFileId { get; set; }
        //[Required]
        //public string LoanApplicationFormFileName { get; set; }
        //[Required]
        //public string LoanApplicationFormFileLink { get; set; }

        public string Comment { get; set; }
    }
}
