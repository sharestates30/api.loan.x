﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.ViewModels
{
    public class StatusGroupViewModel
    {
        public string Group { get; set; }
        public List<StatusViewModel> Status { get; set; }
    }

    public class StatusViewModel
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
