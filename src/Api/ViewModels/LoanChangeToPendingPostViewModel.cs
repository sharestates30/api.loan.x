﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.ViewModels
{
    public class LoanChangeToPendingPostViewModel
    {
        public string Comment { get; set; }
    }

    public class LoanChangeToPendingToAuthorizePostViewModel
    {
        public string Comment { get; set; }
    }
}
