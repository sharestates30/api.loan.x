﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.ViewModels
{
    public class LoanSellerAttorneyPatchViewModel
    {
        public string SellerLawFirm { get; set; }
        public string SellerAttorney { get; set; }
        public string SellerAddress { get; set; }
        public string SellerCountry { get; set; }
        public string SellerCity { get; set; }
        public string SellerState { get; set; }
        public string SellerZipCode { get; set; }
        public string SellerPhone { get; set; }
        public string SellerEmail { get; set; }
    }
}
