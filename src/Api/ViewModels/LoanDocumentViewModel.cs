﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Entity;

namespace Api.ViewModels
{
    public class LoanDocumentViewModel
    {
        public Guid LoanId { get; set; }
        public string DocumentName { get; set; }
        public string DocumentCode { get; set; }
        public int Status { get; set; }
        public IEnumerable<LoanDocumentFileViewModel> Files { get; set; }
        public bool NeedValidation { get; set; }
        public int InternalDocument { get; set; }
        public bool AllowShared { get; set; }
        public int Order { get; set; }
    }

    public class LoanDocumentFileViewModel
    {
        public string FileId { get; set; }
        public string FileName { get; set; }
        public string FileLink { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedByName { get; set; }
        public string CreatedBy { get; set; }
    }
}
