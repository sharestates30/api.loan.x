﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.ViewModels
{
    public class LoanBuyerAttorneyPatchViewModel
    {
        public string BuyerLawFirm { get; internal set; }
        public string BuyerAttorney { get; internal set; }
        public string BuyerAddress { get; internal set; }
        public string BuyerCountry { get; internal set; }
        public string BuyerCity { get; internal set; }
        public string BuyerState { get; internal set; }
        public string BuyerZipCode { get; internal set; }
        public string BuyerPhone { get; internal set; }
        public string BuyerEmail { get; internal set; }
    }
}
