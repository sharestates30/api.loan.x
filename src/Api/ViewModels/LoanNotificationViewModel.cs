﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.ViewModels
{
    public class LoanNotificationViewModel
    {
        public long NotificationId { get; set; }
        public Guid LoanId { get; set; }
        public Guid BorrowerId { get; set; }
        public string BorrowerName { get; set; }
        public Guid? BrokerId { get; set; }
        public string BrokerName { get; set; }
        public string Content { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedByName { get; set; }
        public DateTime CreatedOn { get; set; }
        public string Address { get; set; }
        public bool IsPendingToView { get; set; }
    }
}
