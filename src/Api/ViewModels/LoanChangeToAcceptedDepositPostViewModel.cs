﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Api.ViewModels
{
    public class LoanChangeToAcceptedDepositPostViewModel
    {
        /// <summary>
        /// Documents : Credit Authorization Form
        /// </summary>
        [Required]
        public string CreditAuthorizationFormFileId { get; set; }
        [Required]
        public string CreditAuthorizationFormFileName { get; set; }
        [Required]
        public string CreditAuthorizationFormFileLink { get; set; }

        /// <summary>
        /// Documents : Credit Card Authorization Form
        /// </summary>
        [Required]
        public string CreditCardAuthorizationFormFileId { get; set; }
        [Required]
        public string CreditCardAuthorizationFormFileName { get; set; }
        [Required]
        public string CreditCardAuthorizationFormFileLink { get; set; }

        /// <summary>
        /// Documents : Credit Card Deposit Receipt
        /// </summary>
        [Required]
        public string CreditCardDepositReceiptFileId { get; set; }
        [Required]
        public string CreditCardDepositReceiptFileName { get; set; }
        [Required]
        public string CreditCardDepositReceiptFileLink { get; set; }

        public bool Approve { get; set; }

        public string Comment { get; set; }
    }
}
