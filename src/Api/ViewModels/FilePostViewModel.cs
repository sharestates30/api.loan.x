﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.ViewModels
{
    public class FilePostViewModel
    {
        public string FileId { get; set; }
        public string FileName { get; set; }
        public string FileLink { get; set; }
    }

    public class FileViewModel
    {
        public string FileId { get; set; }
        public string FileName { get; set; }
        public string FileLink { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
