﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.ViewModels
{
    public class ParameterGroupViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
