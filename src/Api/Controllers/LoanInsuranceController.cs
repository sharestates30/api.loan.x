﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Entity;
using Api.ViewModels;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Core;
using Microsoft.EntityFrameworkCore;
using Api.Services;

namespace Api.Controllers
{
    public partial class LoanController
    {

        [HttpGet("{id}/insurance")]
        public async Task<IActionResult> GetInsurance(Guid id)
        {
            using (var context = this._dbContext)
            {
                var model = await context
                                .Insurances.Where(c => c.LoanId.Equals(id))
                                .Select(c => new LoanInsuranceViewModel
                                {
                                    LoanId = c.LoanId,
                                    IsHazardInsuranceRequired = c.IsHazardInsuranceRequired,
                                    HazardInsurancePolicyFileId = c.HazardInsurancePolicyFileId,
                                    HazardInsurancePolicyFileName = c.HazardInsurancePolicyFileName,
                                    HazardInsurancePolicyFileLink = c.HazardInsurancePolicyFileLink,
                                    HazardInsuranceAmount = c.HazardInsuranceAmount,
                                    HazardInsuranceGoodThroughDate = c.HazardInsuranceGoodThroughDate,
                                    HazardInsurancePaidReceiptFileId = c.HazardInsurancePaidReceiptFileId,
                                    HazardInsurancePaidReceiptFileName = c.HazardInsurancePaidReceiptFileName,
                                    HazardInsurancePaidReceiptFileLink = c.HazardInsurancePaidReceiptFileLink,
                                    IsFloodInsuranceRequired = c.IsFloodInsuranceRequired,
                                    FloodInsurancePolicyFileId = c.FloodInsurancePolicyFileId,
                                    FloodInsurancePolicyFileName = c.FloodInsurancePolicyFileName,
                                    FloodInsurancePolicyFileLink = c.FloodInsurancePolicyFileLink,
                                    FloodInsuranceAmount = c.FloodInsuranceAmount,
                                    FloodInsuranceGoodThroughDate = c.FloodInsuranceGoodThroughDate,
                                    FloodInsurancePaidReceiptFileId = c.FloodInsurancePaidReceiptFileId,
                                    FloodInsurancePaidReceiptFileName = c.FloodInsurancePaidReceiptFileName,
                                    FloodInsurancePaidReceiptFileLink = c.FloodInsurancePaidReceiptFileLink
                                }).FirstOrDefaultAsync();

                return this.Ok(model);
            }
        }

        [HttpPost("{id}/insurance")]
        public async Task<IActionResult> PostInsurance(Guid id, [FromBody] LoanInsurancePostViewModel model)
        {
            using (var context = this._dbContext)
            {
                var entity = await context
                                .Insurances
                                .FirstOrDefaultAsync(c => c.LoanId.Equals(id));

                var isNew = false;
                if (entity == null) {

                    entity = new Insurance();
                    entity.LoanId = id;
                    isNew = true;
                }

                entity.IsHazardInsuranceRequired = model.IsHazardInsuranceRequired;
                entity.HazardInsurancePolicyFileId = model.HazardInsurancePolicyFileId;
                entity.HazardInsurancePolicyFileName = model.HazardInsurancePolicyFileName;
                entity.HazardInsurancePolicyFileLink = model.HazardInsurancePolicyFileLink;
                entity.HazardInsuranceAmount = model.HazardInsuranceAmount;
                entity.HazardInsuranceGoodThroughDate = model.HazardInsuranceGoodThroughDate;
                entity.HazardInsurancePaidReceiptFileId = model.HazardInsurancePaidReceiptFileId;
                entity.HazardInsurancePaidReceiptFileName = model.HazardInsurancePaidReceiptFileName;
                entity.HazardInsurancePaidReceiptFileLink = model.HazardInsurancePaidReceiptFileLink;
                entity.IsFloodInsuranceRequired = model.IsFloodInsuranceRequired;
                entity.FloodInsurancePolicyFileId = model.FloodInsurancePolicyFileId;
                entity.FloodInsurancePolicyFileName = model.FloodInsurancePolicyFileName;
                entity.FloodInsurancePolicyFileLink = model.FloodInsurancePolicyFileLink;
                entity.FloodInsuranceAmount = model.FloodInsuranceAmount;
                entity.FloodInsuranceGoodThroughDate = model.FloodInsuranceGoodThroughDate;
                entity.FloodInsurancePaidReceiptFileId = model.FloodInsurancePaidReceiptFileId;
                entity.FloodInsurancePaidReceiptFileName = model.FloodInsurancePaidReceiptFileName;
                entity.FloodInsurancePaidReceiptFileLink = model.FloodInsurancePaidReceiptFileLink;

                if (isNew)
                {
                    entity.CreatedBy = this.SubjectId;
                    entity.CreatedByName = this.SubjectName;
                    entity.CreatedOn = DateTime.UtcNow;

                    context.Insurances.Add(entity);
                }
                else {
                    entity.UpdatedBy = this.SubjectId;
                    entity.UpdatedByName = this.SubjectName;
                    entity.UpdatedOn = DateTime.UtcNow;

                    context.Insurances.Update(entity);
                }

                await context.SaveChangesAsync();

                return this.Ok();
            }
        }

    }
}

