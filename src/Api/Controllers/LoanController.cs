﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Entity;
using Api.ViewModels;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Core;
using Microsoft.EntityFrameworkCore;
using Api.Services;
using Api.Extensions;
using Microsoft.AspNetCore.Authorization;

namespace Api.Controllers
{
    [Route("[controller]")]
    [Authorize]
    public partial class LoanController : BaseController
    {
        private readonly LoanDbContext _dbContext;
        private readonly IHostingEnvironment _env;
        private readonly ISerializeDataService _serializeDataService;
        private readonly IQueueService _queueService;
        private readonly ILoanStatus _loanStatus;
        private readonly IViewRenderService _viewRenderService;

        public LoanController(LoanDbContext dbContext, IHostingEnvironment env, ISerializeDataService serializeDataService, IQueueService queueService, ILoanStatus loanStatus, IViewRenderService viewRenderService)
        {
            this._dbContext = dbContext;
            this._env = env;
            this._serializeDataService = serializeDataService;
            this._queueService = queueService;
            this._loanStatus = loanStatus;
            this._viewRenderService = viewRenderService;
        }
        

        [HttpGet]
        public async Task<IActionResult> Get(
                [FromQuery(Name = "processStatus")]string processStatus,
                [FromQuery(Name = "borrowerId")]string borrowerId,
                [FromQuery(Name = "brokerId")]string brokerId)
        {

            using (var context = this._dbContext)
            {
                var model = (from c in context.Loans
                             where !c.IsPipeline
                             orderby c.SubmittedDay descending
                             select new LoanListViewModel
                             {
                                 LoanId = c.LoanId,
                                 LoanNumber = c.LoanNumber,
                                 TransactionType = (int)c.TransactionType,
                                 TransactionTypeName = c.TransactionType.ToString(),
                                 LoanPurposeType = (int)c.LoanPurposeType,
                                 LoanPurposeTypeName = c.LoanPurposeType.ToString(),
                                 PurchasePrice = c.PurchasePrice,
                                 ConstructionBudget = c.ConstructionBudget,
                                 ConstructionLoanRequest = c.ConstructionLoanRequest,
                                 AquisitionLoanRequest = c.AquisitionLoanRequest,
                                 LoanTermRequest = c.LoanTermRequest,
                                 HasRehabComponent = c.HasRehabComponent,
                                 HasSalesRepresentative = c.HasSalesRepresentative,
                                 IsApproved = c.IsApproved,
                                 IsPipeline = c.IsPipeline,
                                 OriginalPurchaseDate = c.OriginalPurchaseDate,
                                 OriginalPurchasePrice = c.OriginalPurchasePrice,
                                 AsIsValue = c.AsIsValue,
                                 FinalValue = c.FinalValue,
                                 DesiredFundingDate = c.DesiredFundingDate,
                                 Address = c.Address,
                                 Country = c.Country,
                                 City = c.City,
                                 State = c.State,
                                 ZipCode = c.ZipCode,
                                 BorrowerName = c.BorrowerName,
                                 BrokerEntityName = c.BrokerEntityName,
                                 BorrowerCreditScore = c.BorrowerCreditScore,
                                 BorrowerId = c.BorrowerId,
                                 BrokerId = c.BrokerId,
                                 SubmittedDay = c.SubmittedDay,
                                 ProcessStatus = c.ProcessStatus,
                                 BrokerAgentName = c.BrokerAgentName,
                                 BorrowerEntityName = c.BorrowerEntityName,
                                 TermSheetStatus = c.TermSheetStatus,
                                 CreatedOn = c.CreatedOn
                             });


                if (!string.IsNullOrEmpty(borrowerId))
                {
                    model = model.Where(c => c.BorrowerId.Equals(new Guid(borrowerId)));
                }

                if (!string.IsNullOrEmpty(brokerId))
                {
                    model = model.Where(c => c.BrokerId.Equals(new Guid(brokerId)));
                }

                if (!string.IsNullOrEmpty(processStatus))
                {

                    var processStatusList = processStatus.Split(',');

                    model = model.Where(c => processStatusList.Contains(c.ProcessStatus));

                }

                var result = await model.ToListAsync();
                var status = await this._loanStatus.GetStatus();

                foreach (var item in result)
                {
                    var statusValue = status.FirstOrDefault(c => c.Value.Equals(item.ProcessStatus));
                    var termSheetStatusValue = status.FirstOrDefault(c => c.Value.Equals(item.TermSheetStatus));
                    if (statusValue != null)
                    {
                        item.ProcessStatusName = statusValue.Name;
                    }

                    if (termSheetStatusValue != null)
                    {
                        item.TermSheetStatusName = termSheetStatusValue.Name;
                    }
                }



                return this.Ok(result);
            }
        }
        
        [HttpGet("closed")]
        public async Task<IActionResult> GetClosed([FromQuery(Name = "borrowerId")]string borrowerId,
                [FromQuery(Name = "brokerId")]string brokerId)
        {
            var processStatus = string.Join(",", Constants.LoanStatus.Closed);

            using (var context = this._dbContext)
            {
                var model = (from c in context.Loans
                             where c.IsPipeline && c.IsApproved
                             orderby c.SubmittedDay descending
                             select new LoanListViewModel
                             {
                                 LoanId = c.LoanId,
                                 LoanNumber = c.LoanNumber,
                                 TransactionType = (int)c.TransactionType,
                                 TransactionTypeName = c.TransactionType.ToString(),
                                 LoanPurposeType = (int)c.LoanPurposeType,
                                 LoanPurposeTypeName = c.LoanPurposeType.ToString(),
                                 PurchasePrice = c.PurchasePrice,
                                 ConstructionBudget = c.ConstructionBudget,
                                 ConstructionLoanRequest = c.ConstructionLoanRequest,
                                 AquisitionLoanRequest = c.AquisitionLoanRequest,
                                 LoanTermRequest = c.LoanTermRequest,
                                 HasRehabComponent = c.HasRehabComponent,
                                 HasSalesRepresentative = c.HasSalesRepresentative,
                                 IsApproved = c.IsApproved,
                                 IsPipeline = c.IsPipeline,
                                 OriginalPurchaseDate = c.OriginalPurchaseDate,
                                 OriginalPurchasePrice = c.OriginalPurchasePrice,
                                 AsIsValue = c.AsIsValue,
                                 FinalValue = c.FinalValue,
                                 DesiredFundingDate = c.DesiredFundingDate,
                                 Address = c.Address,
                                 Country = c.Country,
                                 City = c.City,
                                 State = c.State,
                                 ZipCode = c.ZipCode,
                                 BorrowerName = c.BorrowerName,
                                 BrokerEntityName = c.BrokerEntityName,
                                 BorrowerCreditScore = c.BorrowerCreditScore,
                                 BorrowerId = c.BorrowerId,
                                 BrokerId = c.BrokerId,
                                 SubmittedDay = c.SubmittedDay,
                                 ProcessStatus = c.ProcessStatus,
                                 BrokerAgentName = c.BrokerAgentName,
                                 BorrowerEntityName = c.BorrowerEntityName,
                                 TermSheetStatus = c.TermSheetStatus,
                                 CreatedOn = c.CreatedOn
                             });


                if (!string.IsNullOrEmpty(borrowerId))
                {
                    model = model.Where(c => c.BorrowerId.Equals(new Guid(borrowerId)));
                }

                if (!string.IsNullOrEmpty(brokerId))
                {
                    model = model.Where(c => c.BrokerId.Equals(new Guid(brokerId)));
                }

                if (!string.IsNullOrEmpty(processStatus))
                {
                    var processStatusList = processStatus.Split(',');

                    model = model.Where(c => processStatusList.Contains(c.ProcessStatus));
                }

                var result = await model.ToListAsync();
                var status = await this._loanStatus.GetStatus();

                foreach (var item in result)
                {
                    var statusValue = status.FirstOrDefault(c => c.Value.Equals(item.ProcessStatus));
                    var termSheetStatusValue = status.FirstOrDefault(c => c.Value.Equals(item.TermSheetStatus));
                    if (statusValue != null)
                    {
                        item.ProcessStatusName = statusValue.Name;
                    }

                    if (termSheetStatusValue != null)
                    {
                        item.TermSheetStatusName = termSheetStatusValue.Name;
                    }
                }


                return this.Ok(result);
            }
        }


        [HttpGet("auctions")]
        public async Task<IActionResult> GetAuctions([FromQuery(Name = "type")]int type)
        {
            using (var context = this._dbContext)
            {
                var model = (from c in context.Loans
                             orderby c.CreatedOn descending
                             select new LoanAuctionListViewModel
                             {
                                 LoanId = c.LoanId,
                                 LoanNumber = c.LoanNumber,
                                 LoanRequest2 = c.ConstructionLoanRequest,
                                 Country = c.Country,
                                 City = c.City,
                                 State = c.State,
                                 ZipCode = c.ZipCode,
                                 CreatedOn = c.CreatedOn,
                                 TargetClosingDate = c.TargetClosingDate
                             });

                switch ((AuctionTypeEnum)type)
                {
                    case AuctionTypeEnum.Appraiser:
                        model = model.Where(c => !c.AuctionClosedForAppraiser);
                        break;
                    case AuctionTypeEnum.TileCompany:
                        model = model.Where(c => !c.AuctionClosedForTitleCompany);
                        break;
                    case AuctionTypeEnum.Attorney:
                        model = model.Where(c => !c.AuctionClosedForAttorney);
                        break;
                    default:
                        break;
                }

                return this.Ok(await model.ToListAsync());
            }
        }

        [HttpGet("{id}", Name = "GetLoanById")]
        public async Task<IActionResult> GetById(Guid id, [FromQuery(Name = "format")]string format)
        {
            try
            {
                using (var context = this._dbContext)
                {
                    var model = (from c in context.Loans
                                 where c.LoanId.Equals(id)
                                 select new LoanViewModel
                                 {
                                     LoanId = c.LoanId,
                                     LoanNumber = c.LoanNumber,
                                     TransactionType = (int)c.TransactionType,
                                     LoanPurposeType = (int)c.LoanPurposeType, 
                                     LoanPurposeTypeName = c.LoanPurposeType.ToString(),
                                     PurchasePrice = c.PurchasePrice,
                                     ConstructionBudget = c.ConstructionBudget,
                                     ConstructionLoanRequest = c.ConstructionLoanRequest,
                                     LoanTermRequest = c.LoanTermRequest,
                                     AppraisalDateNeeded = c.AppraisalDateNeeded,
                                     AppraisalDateOrdered = c.AppraisalDateOrdered,
                                     AppraisalStatus = c.AppraisalStatus,
                                     AppraiserFee = c.AppraiserFee,
                                     AquisitionLoanRequest = c.AquisitionLoanRequest,
                                     AssignorAffiliateExplanation = c.AssignorAffiliateExplanation,
                                     AssignorName = c.AssignorName,
                                     ContractorAssignedValue = c.ContractorAssignedValue,
                                     ClosingDate = c.ClosingDate,
                                     ClosingTime = c.ClosingTime,
                                     CurrentAssetTypeValue = c.CurrentAssetTypeValue,
                                     DisbursedAmountatClosing = c.DisbursedAmountatClosing,
                                     HasAssignorAffiliate = c.HasAssignorAffiliate,
                                     HasRehabComponent = c.HasRehabComponent,
                                     HasSalesRepresentative = c.HasSalesRepresentative,
                                     EntityId = c.EntityId,
                                     IsApproved = c.IsApproved,
                                     IsPipeline = c.IsPipeline,
                                     EntityName = c.EntityName,
                                     EscrowHoldBack = c.EscrowHoldBack,
                                     NewAssetType = (int)c.NewAssetType,
                                     NewAssetTypeValue = c.NewAssetTypeValue,
                                     IsTOETransaction = c.IsTOETransaction,
                                     Location = c.Location,
                                     MoreTermValue = c.MoreTermValue,
                                     NeedMoreTerm = c.NeedMoreTerm,
                                     OriginalPurchaseDate = c.OriginalPurchaseDate,
                                     OriginalPurchasePrice = c.OriginalPurchasePrice,
                                     OriginationEntity = c.OriginationEntity,
                                     ReasonRejected = c.ReasonRejected,
                                     TargetClosingDate = c.TargetClosingDate,
                                     SalesPersonId = c.SalesPersonId,
                                     SalesPersonName = c.SalesPersonName,
                                     TimeOfClosing = c.TimeOfClosing,
                                     TitleBusinessName = c.TitleBusinessName,
                                     TitleCompanyFirstName = c.TitleCompanyFirstName,
                                     TitleCompanyLastName = c.TitleCompanyLastName,
                                     TitlePropertyNumber = c.TitlePropertyNumber,
                                     TOEClosingDate = c.TOEClosingDate,
                                     TransactionTypeName = c.TransactionType.ToString(),
                                     AsIsValue = c.AsIsValue,
                                     FinalValue = c.FinalValue,
                                     DesiredFundingDate = c.DesiredFundingDate,
                                     IsShortSaleTransactionType = c.IsShortSaleTransactionType,
                                     HasAssignmentFlipType = c.HasAssignmentFlipType,
                                     IsLLCTransactionType = c.IsLLCTransactionType,
                                     Address = c.Address,
                                     Address2 = c.Address2,
                                     Country = c.Country,
                                     City = c.City,
                                     State = c.State,
                                     ZipCode = c.ZipCode,
                                     CurrentAssetType = (int)c.CurrentAssetType,
                                     DevelopmentPhase = (int)c.DevelopmentPhase,
                                     Occupancy = (int)c.Occupancy,
                                     ExitStrategy = (int)c.ExitStrategy,
                                     BorrowerName = c.BorrowerName,
                                     BrokerEntityName = c.BrokerEntityName,
                                     BorrowerCreditScore = c.BorrowerCreditScore,
                                     HasBankruptcyType = c.HasBankruptcyType,
                                     BankruptcyExplanation = c.BankruptcyExplanation,
                                     HasDefaultedLoanType = c.HasDefaultedLoanType,
                                     DefaultedLoanExplanation = c.DefaultedLoanExplanation,
                                     HasFiledForeclosure = c.HasFiledForeclosure,
                                     FiledForeclosureExplanation = c.FiledForeclosureExplanation,
                                     HasOutstandingJudgmentsType = c.HasOutstandingJudgmentsType,
                                     OutstandingJudgmentsExplanation = c.OutstandingJudgmentsExplanation,
                                     AppraiserId = c.AppraiserId,
                                     AppraisalCompanyName = c.AppraisalCompanyName,
                                     AppraiserName = c.AppraiserName,

                                     BorrowerId = c.BorrowerId,
                                     BrokerId = c.BrokerId,
                                     BrokerAgentName = c.BrokerAgentName,
                                     SubmittedDay = c.SubmittedDay,
                                     ProcessStatus = c.ProcessStatus,
                                     BorrowerEntityName = c.BorrowerEntityName,
                                     TermSheetStatus = c.TermSheetStatus,
                                     TitleCompanyId = c.TitleCompanyId,
                                     BankruptcyYears = c.BankruptcyYears,
                                 }).FirstOrDefault();

                    if (model == null)
                        return this.NotFound();

                    var status = await this._loanStatus.GetStatus();

                    var statusValue = status.FirstOrDefault(c => c.Value.Equals(model.ProcessStatus));
                    var termSheetStatusValue = status.FirstOrDefault(c => c.Value.Equals(model.TermSheetStatus));
                    if (statusValue != null)
                    {
                        model.ProcessStatusName = statusValue.Name;
                    }

                    if (termSheetStatusValue != null)
                    {
                        model.TermSheetStatusName = termSheetStatusValue.Name;
                    }

                    if (format != null && format.Equals("pdf", StringComparison.OrdinalIgnoreCase))
                    {
                        //var html = await _viewRenderService.RenderToStringAsync("Loan/View", model);
                        //var pdf = SharestatesConverter.ConvertHtmlToPDF(html);

                        //return this.File(pdf, "application/pdf");

                        return View("View", model);
                    }


                    return this.Ok(model);

                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        [HttpPost()]
        public async Task<IActionResult> Post([FromBody] LoanPostViewModel model)
        {
            if (!ModelState.IsValid)
                return this.BadRequest(ModelState);

            // Generate Load Number.
            
            var loadNumber = string.Format("{0:D19}", DateTime.MaxValue.Ticks - DateTime.UtcNow.Ticks);

            using (var context = this._dbContext)
            {
                var loanId = model.LoanId ?? Guid.NewGuid();

                var documents = context.Documents.Select(c => new LoanDocument
                {
                    LoanId = loanId,
                    DocumentId = c.DocumentId,
                    CreatedBy = this.SubjectId,
                    CreatedByName = this.SubjectName,
                    CreatedOn = DateTime.UtcNow
                }).ToList();

                var entity = new Loan
                {
                    LoanId = loanId,
                    LoanNumber = loadNumber,

                    LoanPurposeType = (LoanPurposeTypeEnum)model.LoanPurposeType,
                    TransactionType = (TransactionTypeEnum)model.TransactionType,
                    HasSalesRepresentative = model.HasSalesRepresentative,
                    SalesPersonId = model.SalesPersonId,
                    SalesPersonName = model.SalesPersonName,
                    HasRehabComponent = model.HasRehabComponent,
                    PurchasePrice = model.PurchasePrice,
                    ConstructionBudget = model.ConstructionBudget,
                    AsIsValue = model.AsIsValue,
                    FinalValue = model.FinalValue,
                    AquisitionLoanRequest = model.AquisitionLoanRequest,
                    ConstructionLoanRequest = model.ConstructionLoanRequest,
                    OriginalPurchaseDate = model.OriginalPurchaseDate,
                    OriginalPurchasePrice = model.OriginalPurchasePrice,
                    LoanTermRequest = model.LoanTermRequest,
                    NeedMoreTerm = model.NeedMoreTerm,
                    MoreTermValue = model.MoreTermValue,
                    DesiredFundingDate = model.DesiredFundingDate,
                    IsTOETransaction = model.IsTOETransaction,
                    TOEClosingDate = model.TOEClosingDate,
                    IsShortSaleTransactionType = model.IsShortSaleTransactionType,
                    HasAssignmentFlipType = model.HasAssignmentFlipType,
                    ContractorAssignedValue = model.ContractorAssignedValue,
                    AssignorName = model.AssignorName,
                    HasAssignorAffiliate = model.HasAssignorAffiliate,
                    AssignorAffiliateExplanation = model.AssignorAffiliateExplanation,
                    IsLLCTransactionType = model.IsLLCTransactionType,
                    EntityId = model.EntityId,
                    EntityName = model.EntityName,

                    Address = model.Address,
                    Address2 = model.Address2,
                    Country = model.Country,
                    City = model.City,
                    State = model.State,
                    ZipCode = model.ZipCode,
                    CurrentAssetType = (AssetTypeEnum)model.CurrentAssetType,
                    CurrentAssetTypeValue = model.CurrentAssetTypeValue,
                    Occupancy = (OccupancyTypeEnum)model.Occupancy,
                    NewAssetType = (AssetTypeEnum)model.NewAssetType,
                    NewAssetTypeValue = model.NewAssetTypeValue,
                    DevelopmentPhase = (DevelopmentPhaseTypeEnum)model.DevelopmentPhase,
                    ExitStrategy = (ExitStrategyTypeEnum)model.ExitStrategy,

                    BrokerId = model.BrokerId,
                    BrokerAgentName = model.BrokerAgentName,
                    BrokerEntityName = model.BrokerEntityName,

                    BorrowerId = model.BorrowerId,
                    BorrowerName = model.BorrowerName,
                    BorrowerEntityName = model.BorrowerEntityName,
                    BorrowerCreditScore = model.BorrowerCreditScore,

                    HasBankruptcyType = model.HasBankruptcyType,
                    BankruptcyExplanation = model.BankruptcyExplanation,
                    BankruptcyYears = model.BankruptcyYears,

                    HasDefaultedLoanType = model.HasDefaultedLoanType,
                    DefaultedLoanExplanation = model.DefaultedLoanExplanation,

                    HasFiledForeclosure = model.HasFiledForeclosure,
                    FiledForeclosureExplanation = model.FiledForeclosureExplanation,

                    HasOutstandingJudgmentsType = model.HasOutstandingJudgmentsType,
                    OutstandingJudgmentsExplanation = model.OutstandingJudgmentsExplanation,

                    ProcessStatus = Constants.LoanStatus.Draft,
                    CreatedBy = this.SubjectId,
                    CreatedByName = this.SubjectName,
                    CreatedOn = DateTime.UtcNow,
                    Insurance = new Insurance()
                    {
                        LoanId = loanId,
                        CreatedBy = this.SubjectId,
                        CreatedByName = this.SubjectName,
                        CreatedOn = DateTime.UtcNow
                    },
                    TermSheet = new TermSheet()
                    {
                        LoanId = loanId,
                        CreatedBy = this.SubjectId,
                        CreatedByName = this.SubjectName,
                        CreatedOn = DateTime.UtcNow
                    },
                    Documents = documents,
                    IsPipeline = false,
				};

                if (model.AdditionalProperties != null) {
                    foreach (var item in model.AdditionalProperties)
                    {
                        entity.AddAdditionalProperties(item.Address, item.State, item.City, item.ZipCode, item.Country, this.SubjectId, this.SubjectName);
                    }
                }

                var loanSerialize = this._serializeDataService.SerializeLoan(entity);
                entity.AddLog(Constants.ActionTypeName.New, loanSerialize, string.Empty, this.SubjectId, this.SubjectName);

                context.Loans.Add(entity);
                try
                {
                    await context.SaveChangesAsync();
                }
                catch (Exception ex)
                {
                    throw;
                }

                return this.Created(Url.RouteUrl("GetLoanById", new { id = entity.LoanId }), new { });

            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(Guid id, [FromBody]LoanPostViewModel model)
        {
            using (var context = this._dbContext)
            {
                var entity = context.Loans
                                    .Include(c => c.Logs)
                                    .Include(c=> c.AdditionalProperties)
                                    .FirstOrDefault(c => c.LoanId.Equals(id));

                if (entity == null)
                    return this.NotFound();

                var currentLoanSerialize = this._serializeDataService.SerializeLoan(entity);

                entity.LoanPurposeType = (LoanPurposeTypeEnum)model.LoanPurposeType;
                entity.TransactionType = (TransactionTypeEnum)model.TransactionType;
                entity.HasSalesRepresentative = model.HasSalesRepresentative;
                entity.SalesPersonId = model.SalesPersonId;
                entity.SalesPersonName = model.SalesPersonName;
                entity.HasRehabComponent = model.HasRehabComponent;
                entity.PurchasePrice = model.PurchasePrice;
                entity.ConstructionBudget = model.ConstructionBudget;
                entity.AsIsValue = model.AsIsValue;
                entity.FinalValue = model.FinalValue;
                entity.AquisitionLoanRequest = model.AquisitionLoanRequest;
                entity.ConstructionLoanRequest = model.ConstructionLoanRequest;
                entity.OriginalPurchaseDate = model.OriginalPurchaseDate;
                entity.OriginalPurchasePrice = model.OriginalPurchasePrice;
                entity.LoanTermRequest = model.LoanTermRequest;
                entity.NeedMoreTerm = model.NeedMoreTerm;
                entity.MoreTermValue = model.MoreTermValue;
                entity.DesiredFundingDate = model.DesiredFundingDate;
                entity.IsTOETransaction = model.IsTOETransaction;
                entity.TOEClosingDate = model.TOEClosingDate;
                entity.IsShortSaleTransactionType = model.IsShortSaleTransactionType;
                entity.HasAssignmentFlipType = model.HasAssignmentFlipType;
                entity.ContractorAssignedValue = model.ContractorAssignedValue;
                entity.AssignorName = model.AssignorName;
                entity.HasAssignorAffiliate = model.HasAssignorAffiliate;
                entity.AssignorAffiliateExplanation = model.AssignorAffiliateExplanation;
                entity.IsLLCTransactionType = model.IsLLCTransactionType;
                entity.EntityId = model.EntityId;
                entity.EntityName = model.EntityName;

                entity.Address = model.Address;
                entity.Address2 = model.Address2;
                entity.Country = model.Country;
                entity.City = model.City;
                entity.State = model.State;
                entity.ZipCode = model.ZipCode;
                entity.CurrentAssetType = (AssetTypeEnum)model.CurrentAssetType;
                entity.CurrentAssetTypeValue = model.CurrentAssetTypeValue;
                entity.Occupancy = (OccupancyTypeEnum)model.Occupancy;
                entity.NewAssetType = (AssetTypeEnum)model.NewAssetType;
                entity.NewAssetTypeValue = model.NewAssetTypeValue;
                entity.DevelopmentPhase = (DevelopmentPhaseTypeEnum)model.DevelopmentPhase;
                entity.ExitStrategy = (ExitStrategyTypeEnum)model.ExitStrategy;

                entity.BrokerId = model.BrokerId;
                entity.BrokerAgentName = model.BrokerAgentName;
                entity.BrokerEntityName = model.BrokerEntityName;

                entity.BorrowerId = model.BorrowerId;
                entity.BorrowerName = model.BorrowerName;
                entity.BorrowerEntityName = model.BorrowerEntityName;
                entity.BorrowerCreditScore = model.BorrowerCreditScore;

                entity.HasBankruptcyType = model.HasBankruptcyType;
                entity.BankruptcyExplanation = model.BankruptcyExplanation;
                entity.BankruptcyYears = model.BankruptcyYears;

                entity.HasDefaultedLoanType = model.HasDefaultedLoanType;
                entity.DefaultedLoanExplanation = model.DefaultedLoanExplanation;

                entity.HasFiledForeclosure = model.HasFiledForeclosure;
                entity.FiledForeclosureExplanation = model.FiledForeclosureExplanation;

                entity.HasOutstandingJudgmentsType = model.HasOutstandingJudgmentsType;
                entity.OutstandingJudgmentsExplanation = model.OutstandingJudgmentsExplanation;
                
                
				entity.BankruptcyYears = model.BankruptcyYears;

                if (model.AdditionalProperties != null)
                {
                    foreach (var item in model.AdditionalProperties)
                    {
                        entity.AddAdditionalProperties(item.Address, item.State, item.City, item.ZipCode, item.Country, this.SubjectId, this.SubjectName);
                    }
                }

                context.Loans.Update(entity);

                var newLoanSerialize = this._serializeDataService.SerializeLoan(entity);
                entity.AddLog(Constants.ActionTypeName.Update, newLoanSerialize, currentLoanSerialize, this.SubjectId, this.SubjectName);

                await context.SaveChangesAsync();

                return this.Ok();
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            using (var context = this._dbContext)
            {
                var entity = context
                                .Loans
                                .Include(c => c.Logs)
                                .Include(c => c.Trackings)
                                .FirstOrDefault(c => c.LoanId.Equals(id));

                if (entity == null)
                    return this.NotFound();

                context.Loans.Remove(entity);
                await context.SaveChangesAsync();

                return this.NoContent();
            }
        }
        
    }
}

