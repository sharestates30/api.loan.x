﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Entity;
using Api.ViewModels;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Core;
using Microsoft.EntityFrameworkCore;
using Api.Services;

namespace Api.Controllers
{
    public partial class LoanController
    {


        [HttpGet("{id}/trackings")]
        public async Task<IActionResult> GetTracking(Guid id)
        {
            using (var context = this._dbContext)
            {
                var model = await (from c in context.LoanTrackings
                             where c.LoanId.Equals(id)
                             orderby c.CreatedOn descending
                             select new LoanTrackingViewModel
                             {
                                 LoanId = c.LoanId,
                                 Code = c.Code,
                                 Content = c.Content,
                                 CreatedBy = c.CreatedBy,
                                 CreatedByName = c.CreatedByName,
                                 LoanTrackingId = c.LoanTrackingId,
                                 Status = c.Status,
                                 StatusName = c.Status.ToString(),
                                 CreatedOn = c.CreatedOn
                             }).ToListAsync();

                return this.Ok(model);
            }
        }

    }
}

