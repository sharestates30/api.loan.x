﻿using Core;
using Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Services
{
    public class LoanStatus : ILoanStatus
    {
        private readonly LoanDbContext _dbContext;

        public LoanStatus(LoanDbContext dbContext)
        {
            this._dbContext = dbContext;
        }

        public async Task<IReadOnlyList<Status>> GetStatus(string code)
        {
            var codeLists = new List<string>()
            {
                Constants.ParameterGroup.OpenerStatusProject,
                Constants.ParameterGroup.OriginatorStatusProject,
                Constants.ParameterGroup.ProcessingDepartmentStatusProject,
                Constants.ParameterGroup.UnderwritingStatusProject,
                Constants.ParameterGroup.ClosingDeskStatusProject,
                Constants.ParameterGroup.PostClosingStatusProject,
            };

            var status = await _dbContext.Parameters.Where(c => codeLists.Contains(c.Code)).ToListAsync();

            return status.Where(c => c.Code.Equals(code)).Select(c=> new Status { Value = c.Value, Name = c.Name }).ToList();
        }

        public async Task<IReadOnlyList<Status>> GetStatus()
        {
            var codeLists = new List<string>()
            {
                Constants.ParameterGroup.OpenerStatusProject,
                Constants.ParameterGroup.OriginatorStatusProject,
                Constants.ParameterGroup.ProcessingDepartmentStatusProject,
                Constants.ParameterGroup.UnderwritingStatusProject,
                Constants.ParameterGroup.ClosingDeskStatusProject,
                Constants.ParameterGroup.PostClosingStatusProject,
            };

            var status = await _dbContext.Parameters.Where(c => codeLists.Contains(c.Code)).ToListAsync();

            return status.Select(c => new Status { Group = c.Code, Value = c.Value, Name = c.Name }).ToList();
        }
    }
}
