﻿using Microsoft.AspNetCore.Hosting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Api.Services
{
    public class QueueService : IQueueService
    {
        private readonly CloudQueueServiceProvider _provider;
        private readonly IHostingEnvironment _environment;
        public QueueService(IHostingEnvironment environment)
        {
            this._provider = new CloudQueueServiceProvider(environment);
        }

        public async Task SendMessage(string queueName, object message)
        {
            var queue = this._provider.GetQueue("dfw", queueName);
            await this._provider.SendQueue(queue, new CloudMessageQueue(message));
        }

    }
}
