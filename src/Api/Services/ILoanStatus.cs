﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Services
{
    public interface ILoanStatus
    {
        Task<IReadOnlyList<Status>> GetStatus(string code);
        Task<IReadOnlyList<Status>> GetStatus();
    }

    public class Status {
        public string Group { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
