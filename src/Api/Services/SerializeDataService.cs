﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Entity;

namespace Api.Services
{
    public class SerializeDataService : ISerializeDataService
    {
        public string SerializeLoan(Loan loan)
        {
            try
            {
                var json = Newtonsoft.Json.JsonConvert.SerializeObject(loan);

                return json;
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}
