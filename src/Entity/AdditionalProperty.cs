﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entity
{
    public class AdditionalProperty : BaseEntity
    {
        public AdditionalProperty() : base()
        {

        }

        public int AdditionalPropertyId { get; set; }
        public string Address { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }

        public Guid LoanId { get; set; }
        public virtual Loan Loan { get; set; }

    }
}
