﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entity
{
    public class StageRole : BaseEntity
    {
        public int StageRoleId { get; set; }

        public int RoleId { get; set; }
        public virtual Rol Rol { get; set; }

        public int StageId { get; set; }
        public virtual Stage Stage { get; set; }
    }
}
