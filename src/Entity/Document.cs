﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entity
{
    public class Document : BaseEntity
    {
        public int DocumentId { get; set; }
        public string DocumentCode { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public int Order { get; set; }
        public bool NeedValidation { get; set; }
        public bool AllowShared { get; set; }
        public DocumentUploadType InternalDocument { get; set; }

        public virtual ICollection<DocumentStage> Stages { get; set; }
        public virtual ICollection<LoanDocument> LoanDocuments { get; set; }
    }

    public enum DocumentUploadType {
        Internal,
        User,
        Both
    }
}
