﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entity
{
    public class Stage : BaseEntity
    {
        public int StageId { get; set; }
        public string StageCode { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public int StageGroupId { get; set; }
        public virtual StageGroup StageGroup { get; set; }

        public virtual ICollection<StageStatus> Status { get; set; }
        public virtual ICollection<StageRole> Roles { get; set; }
        public virtual ICollection<DocumentStage> Documents { get; set; }
    }
}
